#!/bin/bash

declare win_profile=${WIN_PROFILE:?required WIN_PROFILE}

# create the external .wsl folder
if ! [ -d "$win_profile/.wsl" ] ; then                      
    mkdir -p $win_profile/.wsl
fi

# source bashrc if it exists
if ! cat ~/.profile | grep "wsl_configured" ; then
    cat >> ~/.profile <<EOF

# wsl_configured file : external configuration
# if running bash                  
if [ -n "\$BASH_VERSION" ]; then    
    # include .bashrc if it exists 
    if [ -f "$win_profile/.wsl/.bashrc" ]; then
        . "$win_profile/.wsl/.bashrc"
    fi                             
fi                    
EOF
fi

# source bashrc if it exists
if ! cat $win_profile/.wsl/.bashrc | grep "wsl_configured" ; then
    cat > $win_profile/.wsl/.bashrc <<EOF
# wsl_configured file
# AWS configuration
export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
export AWS_REGION_NAME=$AWS_REGION_NAME
export AWS_OUTPUT_FORMAT=$AWS_OUTPUT_FORMAT

# GitLab configuration
export GIT_TOKEN=$GIT_TOKEN
export GIT_LOGIN=$GIT_LOGIN
export GIT_CRED=\$GIT_LOGIN:\$GIT_TOKEN
EOF
fi