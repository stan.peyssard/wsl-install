#!/bin/bash

# Install python
sudo apt-get update
sudo apt-get install -y python3-pip

# Install awscli
pip3 install --user awscli
. ~/.profile
echo "---> AWS CLI installed - check the version below"
aws --version

# Configure awscli
aws configure >/dev/null <<EOF
${AWS_ACCESS_KEY_ID}
${AWS_SECRET_ACCESS_KEY}
${AWS_REGION_NAME}
${AWS_OUTPUT_FORMAT}
EOF

# Install aws-iam-authenticator and kubectl
curl -o $HOME/.local/bin/aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.13.7/2019-06-11/bin/linux/amd64/aws-iam-authenticator
chmod +x $HOME/.local/bin/aws-iam-authenticator
echo "---> aws-iam-authenticator installed - check the version below"
aws-iam-authenticator version

# kubectl
curl -o $HOME/.local/bin/kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.12.9/2019-06-21/bin/linux/amd64/kubectl
chmod +x $HOME/.local/bin/kubectl
echo "---> kubectl installed - check the version below"
kubectl version 2>/dev/null || true

# Install eksctl
curl --silent --location "https://github.com/weaveworks/eksctl/releases/download/latest_release/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C $HOME/.local/bin

# Install bat
curl -o bat.deb --silent --location "https://github.com/sharkdp/bat/releases/download/v0.11.0/bat_0.11.0_amd64.deb"
sudo dpkg -i bat.deb
rm bat.deb
