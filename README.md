# Windows 10 - Use WSL (Windows Subsystem for Linux)

## Install WSL with ubuntu
- Enable Windows substem for Linux
![images/enable-wsl.png](enable-wsl)

- When it is done, you have to restart your laptop
- Click this link https://aka.ms/wslstore to open the store and choose Ubuntu
- Download the Ubuntu distribution (around 215MB)
- Finally launch the application to install Ubuntu. It may takes a few minutes

## Install tools

- Configure required variables to be installed on system
```shell
# Used to store a .wsl folder and holding a .wsl/bashrc outside WSL
export WIN_PROFILE=/mnt/d/Profiles/<your_username>
# AWS
export AWS_ACCESS_KEY_ID=<YOUR_AWS_ACCESS_KEY_ID>
export AWS_SECRET_ACCESS_KEY=<YOUR_AWS_SECRET_ACCESS_KEY>
export AWS_REGION_NAME=eu-west-1
export AWS_OUTPUT_FORMAT=json
# GitLab
export GIT_LOGIN=<YOUR_GITLAB_USER>
export GIT_TOKEN=<YOUR_GITLAB_ACCESS_TOKEN>
export GIT_CRED=$GIT_LOGIN:$GIT_LOGIN
```

- Apply the configuration
```shell
bash <(curl -s https://gitlab.com/stan.peyssard/wsl-install/raw/master/configure_wsl.sh)
```

- Install tools
```shell
bash <(curl -s https://gitlab.com/stan.peyssard/wsl-install/raw/master/install_tools.sh)
```